package controller;


/**
 * @author Simone
 * the manager of Input (of the bar).
 */
public interface Input {

    /**
     * will call on bar where it has to go.
     */
    void moving();

}
