package utility;

/**
 * @author Simone
 * the Enum with the movement existing in this game.
 */
public enum Direction {
    /**
     * Y movement upward.
     */
    UP,
    /**
     * Y movement downward.
     */
    DOWN,
    /**
     * BAR doesn't move.
     */
    STOP
}
