package utility;

/**
 * @author Simone
 * The Enum with the name of each Background, need to add ".jpg"
 */
public enum Background {
    /**
     * name of image.
     */
    BGTECNO,
    /**
     * name of image.
     */
    BGMARS,
    /**
     * name of image.
     */
    BGSPACE,
    /**
     * name of image.
     */
    BG8K,
    /**
     * name of image.
     */
    BGSOCCER,
    /**
     * name of image.
     */
    BGPYRAMIDS;
}
