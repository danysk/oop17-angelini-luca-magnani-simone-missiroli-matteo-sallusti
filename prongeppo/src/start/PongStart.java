package start;

import view.StartMenu;

final class PongStart {

    private PongStart() { }

    public static void main(final String[] args) {
        new StartMenu().open();
    }
}
